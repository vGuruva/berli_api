<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Signup & Login
Route::post('login', 'Auth\LoginController@login');
Route::post('signup', 'Auth\RegisterController@signup');

// Verification routes
Route::get('activate/{token}', 'Auth\VerificationController@signupActivate');

// Password management
Route::post('password/forgot', 'Auth\ResetPasswordController@create');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


// From here on you need to be logged in to access the below routes
Route::group([
    'middleware' => 'auth:api'
], function () {

// Images routes
    Route::get('import/image/data', 'PictureController@readCSV');
    Route::get('get/all/images', 'PictureController@allImages');
    Route::get('get/image/{id}', 'PictureController@getImage')->where('id', '[0-9]+');
    Route::get('logout', 'Auth\LoginController@logout');
});


