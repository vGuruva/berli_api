<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/', 'PictureController@index')->name('import');
// Route::post('/import_parse', 'PictureController@parseImport')->name('import_parse');
// Route::post('/import_process', 'PictureController@processImport')->name('import_process');