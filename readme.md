# Images: Rest API
This is a laravel powered API used to upload images imformation from a CSV file and saving that information within the api. The data can then be retrieved using the endpoints below:

The following HTTP methods will be available within the API as outlined below:

|     Method           |ASCII                          |HTML                         |
|----------------|-------------------------------|-------------|
|GET            |`/api/resource`            |index resources        |
|GET             |`/api/resource/{id}`|show resource|


|     Method           | URI             |Description
|----------------|-------------------------------|-------------|
|POST			 | `api/signup`   | User sign up, create an account|
|POST             |`api/login`| User login|         
|GET            |`api/activate/{token}`| Activate user account|
|POST             |`api/password/forgot`| Forgot password|
|POST             |`api/password/reset`| Reset password|
|GET             |`api/import/image/data`| Import image data from file|
|GET             |`api/get/all/images`| Get all images|
|GET             |`api/get/image/{id}`| Get image by id|


# Header

All resource request headers must have the following key value pairs to interact with the API:

|     Param           |Value                          |                         |
|----------------|-------------------------------|-------------|
|Content-Type .           |`application/json`            |         |
|Authorization            |`Bearer Token` | Token key generated on user sign in  |


## Request & Response Formats

All requests will be handled according to the standard provided by laravel. For more information visit  [laravel documentation](https://laravel.com/docs/5.8/responses).
>{
	"status" :  true,
	"code" :  200,
	"message" : [],
	"result" : {
	"model" :  { "id": 1, //etc },
}

## Run following command to create personal access client codes
Api uses laravel passort for user authentication [laravel passport](https://laravel.com/docs/5.8/passport)

php artisan passport:install

## Signing up to use api

Sign up with `name`, `email` and `password` to get an activation payload as shown below:
{
    "status": true,
    "message": "You have been registered. Please verify your email address.",
    "token": "http://berl.local/api/activate/0481fc877fae089b0bfd4fe0df7aaa51"
}

## Activate user and get confirmation as shown
{
"status": true,
"messages": "Your account has been successfully activated. You may login."
}

## Login with user to import data and access saved api data using bearer token
Please run  php artisan config:cache and  php artisan cache:clear 
to clear cache


