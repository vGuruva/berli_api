<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\HasApiTokens;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, HasApiTokens;

    public function randHash($len=32)
    {
        return substr(md5(openssl_random_pseudo_bytes(20)),-$len);
    }

    public function login(Request $request)
    {
        $validate = [
            'password' => 'required|string',
            'email' => 'required|string|email'
        ];

        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails()) {
            return $this->throw_error($validator->messages());
        }

        $credentials['password'] = $request->password;
        $credentials['active'] = 1;

        if (!Auth::attempt($credentials))
            return $this->throw_error('Invalid credentials! Please confirm that your details are correct and that you have activated your account.');
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        $token->save();

        $user->save();

        return response()->json([
            'status' => true,
            'token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'messages' => '',
            'result' => [
                'user' => $user,
            ],
        ], 200);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
}
