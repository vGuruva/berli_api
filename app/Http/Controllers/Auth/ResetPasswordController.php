<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Traits\HandleToken;
use Carbon\Carbon;
use App\User;
use App\PasswordReset;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    public function randHash($len=32)
    {
        return substr(md5(openssl_random_pseudo_bytes(20)),-$len);
    }

    public function reset(Request $request)
    {
        
        $request->validate([
            'password' => 'required|string|min:8',
            'token' => 'required|string'
        ]);

        $method = 'email';

        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            [$method, $request->email],
        ])->first();

        if (!$passwordReset) {
            return $this->throw_error('This password reset token is invalid or the email is invalid!');
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return $this->throw_error('Reset token expired. Please request a new reset link.');
        }

        $user = User::where($method, $passwordReset->$method)->first();

        if (!$user) {
            return $this->throw_error('We can\'t find a user  with that email address.');
        }

        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();

        return response()->json([
            'status' => true,
            'messages' => 'Your password has successfully been changed. You may now login with your new password.',
        ], 200);
    }

    public function create(Request $request)
    {

        $method = 'email';

        $validate['email'] = 'required|string|email';

        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails()) {

            return $this->throw_error($validator->messages());
        }

        $user = User::where($method, $request->email)->first();

        if (!$user) {

            return $this->throw_error('We can\'t find a user with that email address.');
        }

        $password_reset = PasswordReset::where('email', '=', $user->email)->first();
        if ($password_reset) {
            $password_reset->delete();
        }

        $password_reset = new PasswordReset([
            'email' => $user->email,
            'token' => $this->randHash(),
        ]);

        $password_reset->save();

        $response['token'] = url('/api/password/reset -> with token: ' . $password_reset->token);

        return response()->json($response, 200);
    }
}
