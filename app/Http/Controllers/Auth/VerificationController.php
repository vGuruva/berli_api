<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\User;
use Illuminate\Http\Request;
class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */


    public function signupActivate($token)
    {

        $user = User::where('activation_token', $token)->first();

        if (!$user) {
            return $this->throw_error('This activation token or the email does not match or is invalid!');
        }
        $user->active = true;;
        $user->email_verified_at = now();
        $user->activation_token = '';

        $user->save();

        return response()->json([
            'status' => true,
            'messages' => 'Your account has been successfully activated. You may login.',
        ], 200);
    }

}
