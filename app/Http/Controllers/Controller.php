<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function apiResponse($data, $entity, $code, $report=null)
    {
        $message = is_null($report) ? 'success' : $report->getMessage();

        return $response = [
            'code' => $code,
            'message' => $message,
            'data' => [ $entity => $data ]
        ]; 

    }

    public function throw_error($message)
    {
        return response()->json([
            'status' => false,
            'message' => $message,
        ], 400);
    }
}
