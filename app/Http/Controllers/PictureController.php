<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Picture;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use Validator;
use Response;
use Storage;
use File;
use Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PictureController extends Controller
{
    public function storeData($data)
    {  
        if(!$data[0] && !$data[1] && !$data[2]) return false;

        $title = strip_tags($data[0]);
        $url = strip_tags($data[1]);
        $description = strip_tags($data[2]);
        $basename = '';
        $isImage = $this->checkImage($url);

        if($isImage){
            $basename = $this->saveUpdateImage($url);
        }
   
        try{
            $image = Picture::updateOrCreate(
                ['title' => trim($title)], 
                [
                    'url' => trim($url),
                    'description' => trim($description),
                    'basename' => $basename
                ]
            );

        }catch(QueryException $e){
            $error = 'Failed to create image due to: '. $e->getMessage();
            return response()->json(['error' => $error], 404);
        }
    }

    public function saveUpdateImage($url){
        $info = pathinfo($url);
        $contents = file_get_contents($url);
        $file = storage_path(). '/app/public/images/' . $info['basename'];
        file_put_contents($file, $contents);
        $uploaded_file = new UploadedFile($file, $info['basename']);
        return $info['basename'];
    }

    public function checkImage($url){
        if (@getimagesize($url)) {
            return true;
        } else {
            return false;
        }
    }

    public function readCSV()
    {
        $filename = 'images_data';
        $path = storage_path() . "/csv/${filename}.csv";
        $reader = new Csv();
        $reader->setDelimiter('|');
        $spreadsheet = $reader->load($path);
        $sheetData = $spreadsheet->getActiveSheet('images_data')->toArray();

        foreach($sheetData as $ki => $data):

            if($ki!=0){
                $this->storeData($data);
            }

        endforeach;

        return response()->json([
            'status' => true,
            'message' => 'Images saved',
        ]);
    }

    public function allImages()
    {
        $images = [];
        $results = [];

        try {
            $images = Picture::all();
            if($images->isEmpty()):
                throw new ModelNotFoundException('No query results for model [App\\Models\\Picture] all');
            endif;
        
            foreach($images as $key => $image):
                $imageUrl = $image->basename? asset('storage/images/'.$image->basename) : 'No image url supplied';
                $desc = $image->description? $image->description: 'No description supplied';
                $title = $image->title? $image->title: 'No title supplied';

                $results[$key]['id'] = $image->id;
                $results[$key]['picture_title'] = $title;
                $results[$key]['picture_url'] = trim($imageUrl);
                $results[$key]['picture_description'] = $desc;
            endforeach;

        } catch (ModelNotFoundException $e) {
            $error = $this->apiResponse($results, 'images', 404, $e);
            return response()->json([
                'error' => $error
            ], 404);
        }

        $response = $this->apiResponse($results,'images', 200);
        return response()->json(compact('response'));
    }

    public function getImage($id)
    {
        $image = [];
        $result = [];

        try {
            $image = Picture::findOrFail($id);

            if($image){
                $imageUrl = $image->basename? asset('storage/images/'.$image->basename) : 'No image url supplied';
                $desc = $image->description? $image->description: 'No description supplied';
                $title = $image->title? $image->title: 'No title supplied';

                $result['id'] = $image->id;
                $result['picture_title'] = $title;
                $result['picture_url'] = trim($imageUrl);
                $result['picture_description'] = $desc;
            }

        } catch (ModelNotFoundException $e) {

            $error = $this->apiResponse($result,'image', 404, $e);
            return response()->json(['error' => $error], 404);

        }

        $response = $this->apiResponse($result,'image', 200);
        return response()->json(compact('response'));

    }


}
